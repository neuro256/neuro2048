﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Direction
{
    LEFT,
    RIGHT,
    DOWN,
    UP
};

public delegate void SwipeEventHandler(Direction direction);

public class SwipeGesture : MonoBehaviour
{
    public event SwipeEventHandler SwipeEvent;
    private Vector2 fingerStart;
    private Vector2 fingerEnd;

    public enum Movement
    {
        Left,
        Right,
        Up,
        Down
    };

    public List<Movement> movements = new List<Movement>();
    public float tolerance = 80.0f;

    private void Update()
    {
        #if UNITY_ANDROID || UNITY_IOS
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerStart = touch.position;
                fingerEnd = touch.position;
            }

            if (touch.phase == TouchPhase.Moved)
            {
                fingerEnd = touch.position;
                if (Mathf.Abs(fingerEnd.x - fingerStart.x) > tolerance || Mathf.Abs(fingerEnd.y - fingerStart.y) > tolerance)
                {
                    if (Mathf.Abs(fingerStart.x - fingerEnd.x) > Mathf.Abs(fingerStart.y - fingerEnd.y))
                    {
                        // Right swipe
                        if ((fingerEnd.x - fingerStart.x) > 0)
                        {
                            movements.Add(Movement.Right);
                            Swipe(Direction.RIGHT);
                        }
                        // Left swipe
                        else
                        {
                            movements.Add(Movement.Left);
                            Swipe(Direction.LEFT);
                        }
                    }
                    //More movement along the Y axis than the X axis
                    else
                    {
                        //Upward Swipe
                        if ((fingerEnd.y - fingerStart.y) > 0)
                        {
                            movements.Add(Movement.Up);
                            Swipe(Direction.UP);
                        }
                        //Downward Swipe
                        else
                        {
                            movements.Add(Movement.Down);
                            Swipe(Direction.DOWN);
                        }
                    }
                    //After the checks are performed, set the fingerStart & fingerEnd to be the same
                    fingerStart = touch.position;
                    //Now let's check if the Movement pattern is what we want
                    //In this example, I'm checking whether the pattern is Left, then Right, then Left again
                    //Debug.Log(CheckForPatternMove(0, 3, new List<Movement>() { Movement.Left, Movement.Right, Movement.Left }));
                }
            }
            if (touch.phase == TouchPhase.Ended)
            {
                fingerStart = Vector2.zero;
                fingerEnd = Vector2.zero;
                movements.Clear();
            }
        }
        #endif
    }

    private void Swipe(Direction direction)
    {
        if (SwipeEvent != null)
            SwipeEvent(direction);
    }

    private bool CheckForPatternMove(int startIndex, int lengthOfPattern, List<Movement> movementToCheck)
    {

        //If the currently stored movements are fewer than the length of the pattern to be detected
        //it can never match the pattern. So, let's get out
        if (lengthOfPattern > movements.Count)
            return false;

        //In case the start index for the check plus the length of the pattern
        //exceeds the movement list's count, it'll throw an exception, so lets get out
        if (startIndex + lengthOfPattern > movements.Count)
            return false;

        //Populate a temporary list with the respective elements
        //from the movement list
        List<Movement> tMovements = new List<Movement>();
        for (int i = startIndex; i < startIndex + lengthOfPattern; i++)
            tMovements.Add(movements[i]);

        //Now check whether the sequence of movements is the same as the pattern you want to check for
        //The SequenceEqual method is in the System.Linq namespace
        return tMovements.SequenceEqual(movementToCheck);
    }
}
