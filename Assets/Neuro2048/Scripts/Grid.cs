﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public delegate void ScoreEventHandler(int value);
public delegate void GameOverEventHandler();

public class Grid : MonoBehaviour
{
    public event ScoreEventHandler ScoreEvent;
    public event GameOverEventHandler GameOverEvent;

    private enum SortingLayer { Tiles, Cells };

    struct MovePositions
    {
        public Vector2 farthest { get; set; }
        public Vector2 next { get; set; }
    }

    private const int m_DirectionsCount = 4;
    private List<Cell> m_Cells;
    private int m_GridSize;
    private bool isGridBlocked = false;
    private int m_Score;

    public bool IsGridBlocked
    {
        get
        {
            return isGridBlocked;
        }
    }

    /// <summary>
    /// Создание игрового поля заданного размера 
    /// </summary>
    /// <param name="gridSize">Размер игрового поля (матрица размера gridSize * gridSize)</param>
    public void Create(int gridSize)
    {
        isGridBlocked = true;
        m_Cells = new List<Cell>(gridSize * gridSize);
        m_GridSize = gridSize;
        m_Score = 0;

        for (int x = 0; x < gridSize; x++)
        {
            for (int y = 0; y < gridSize; y++)
            {
                Vector2 position = x * Vector2.right - y * Vector2.up;
                Cell lCell = Instantiate(Cell.prefab, (Vector3)(Vector2)position + (int)SortingLayer.Cells * Vector3.forward, Quaternion.identity);
                lCell.transform.parent = transform;
                lCell.position = new Vector2(x, y);
                m_Cells.Add(lCell);
            }
        }
        isGridBlocked = false;
    }

    public void Clear()
    {
        if (m_Cells != null)
        {
            foreach (var cell in m_Cells)
            {
                if (cell.tileExist)
                    Destroy(cell.getTile.gameObject);
                Destroy(cell.gameObject);
            }
            m_Cells.Clear();
            m_Cells = null;
        }
    }

    /// <summary>
    /// Добавление тайла на случайной позиции
    /// </summary>
    public void AddRandomTile()
    {
        isGridBlocked = true;
        if (IsCellsAvailable())
        {
            int tileValue = UnityEngine.Random.Range(0.0f, 1.0f) < 0.9 ? 2 : 4;
            Cell randomCell = GetRandomAvailableCell();
            Tile newTile = Instantiate(Tile.prefab, (int)SortingLayer.Tiles * Vector3.forward, Quaternion.identity);
            newTile.Create(randomCell.transform.position, tileValue);
            randomCell.SetTile(newTile);
        }
        isGridBlocked = false;
    }

    /// <summary>
    /// Возвращает первую свободную случайную клетку
    /// </summary>
    /// <returns>Свободная клетка на игровом поле</returns>
    private Cell GetRandomAvailableCell()
    {
        List<Cell> availableCells = GetAvailableCells();
        return availableCells[Mathf.FloorToInt(UnityEngine.Random.Range(0, availableCells.Count))];
    }

    private List<Cell> GetAvailableCells()
    {
        List<Cell> availableCells = new List<Cell>();
        foreach (var cell in m_Cells)
        {
            if (!cell.tileExist)
                availableCells.Add(cell);
        }
        return availableCells;
    }

    /// <summary>
    /// Проверка на существование свободных клеток на игрвом поле
    /// </summary>
    /// <returns>Истина если существуют свободные клетки на игровом поле</returns>
    private bool IsCellsAvailable()
    {
        return GetAvailableCells().Count > 0;
    }

    private bool MovesAvailable()
    {
        return IsCellsAvailable() || TileMatchesAvailable();
    }

    private bool TileMatchesAvailable()
    {
        for (int x = 0; x < m_GridSize; x++)
        {
            for (int y = 0; y < m_GridSize; y++)
            {
                Tile tile = CellContent(new Vector2(x, y));
                if (tile != null)
                {
                    for (int direction = 0; direction < m_DirectionsCount; direction++)
                    {
                        Vector2 vector = GetVector((Direction)direction);
                        Tile otherTile = CellContent(new Vector2(x + vector.x, y + vector.y));
                        if (otherTile != null && otherTile.value == tile.value)
                        {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private bool IsCellAvailable(Vector2 cellPos)
    {
        return !CellOccupied(cellPos);
    }

    private bool CellOccupied(Vector2 cellPos)
    {
        return CellContent(cellPos) != null;
    }

    /// <summary>
    /// Проверка, содержит ли клетка какое-либо значение
    /// </summary>
    /// <param name="cellPos"></param>
    /// <returns></returns>
    private Tile CellContent(Vector2 cellPos)
    {
        if (WithinBounds(cellPos))
        {
            return GetTileByIndex(cellPos);
        }
        else
        {
            return null;
        }
    }

    private bool WithinBounds(Vector2 position)
    {
        return position.x >= 0 && position.x < m_GridSize &&
                position.y >= 0 && position.y < m_GridSize;
    }

    private Tile GetTileByIndex(Vector2 position)
    {
        Cell cell = m_Cells.Where(p => p.position == position).SingleOrDefault();
        if (cell != null)
            return cell.getTile;
        return null;
    }

    private Cell GetCellByIndex(Vector2 position)
    {
        return m_Cells.Where(p => p.position == position).SingleOrDefault();
    }

    public IEnumerator TileMove(Direction direction)
    {
        isGridBlocked = true;

        Vector2 vector = GetVector(direction);

        int[] xTraversal = BuildTraversal(m_GridSize);
        int[] yTraversal = BuildTraversal(m_GridSize);

        if (vector == Vector2.up)
            System.Array.Reverse(yTraversal);
        if (vector == Vector2.right)
            System.Array.Reverse(xTraversal);

        bool moved = false;

        PrepareTiles();

        foreach (int x in xTraversal)
        {
            foreach (int y in yTraversal)
            {
                Vector2 cellPos = new Vector2(x, y);
                Tile tile = CellContent(cellPos);
                if (tile != null)
                {
                    MovePositions positions = FindFarthestPositions(vector, cellPos);

                    Cell oldCell = GetCellByIndex(cellPos);
                    Cell newCellNext = GetCellByIndex(positions.next);
                    Cell newCellFarthest = GetCellByIndex(positions.farthest);


                    if (newCellNext != null && newCellNext.getTile.value == oldCell.getTile.value && !newCellNext.getTile.merged)
                    {
                        Move(oldCell, newCellNext, true);
                        moved = true;
                    }
                    else if (newCellFarthest != null && oldCell != newCellFarthest)
                    {
                        Move(oldCell, newCellFarthest, false);
                        moved = true;
                    }
                }
            }
        }

        yield return StartCoroutine(WaitForTilesMoves());

        if (moved)
        {
            AddRandomTile();
            if (!MovesAvailable())
                GameOver();
        }
        isGridBlocked = false;
    }

    private void PrepareTiles()
    {
        foreach (var cell in m_Cells)
        {
            if (cell.tileExist)
                cell.getTile.merged = false;
        }
    }

    private Vector2 GetVector(Direction direction)
    {
        switch (direction)
        {
            case Direction.LEFT:
                return -Vector2.right;
            case Direction.RIGHT:
                return Vector2.right;
            case Direction.UP:
                return -Vector2.up;
            case Direction.DOWN:
                return Vector2.up;
        }
        return Vector2.zero;
    }

    private int[] BuildTraversal(int size)
    {
        int[] tempArray = new int[size];
        for (int i = 0; i < size; i++)
        {
            tempArray[i] = i;
        }
        return tempArray;
    }

    private void Move(Cell oldCell, Cell newCell, bool merged)
    {
        Tile tile = oldCell.getTile;
        if (merged)
        {
            tile.merged = true;
            // Уничтожаем замещаемый тайл
            StartCoroutine(newCell.getTile.Destroy());
            // Замещаем его перемещаемым тайлом
            oldCell.SetTile(null);
            newCell.SetTile(tile);
            // Перемещаем тайл на новую позицию и изменяем его значение 
            StartCoroutine(tile.Move(newCell.transform.position));
            // Зачисление очков и вычисление нового значения 
            int value = newCell.getTile.value;
            ScoreEventChanged(value);
            newCell.getTile.ChangeValue(value * 2);
            StartCoroutine(tile.Scale());
        }
        else
        {
            oldCell.SetTile(null);
            newCell.SetTile(tile);
            StartCoroutine(tile.Move(newCell.transform.position));
        }
    }

    private IEnumerator WaitForTilesMoves()
    {
        do
        {
            yield return null;
            bool tilesAreMoving = false;
            foreach (var cell in m_Cells)
            {
                if (cell.tileExist && cell.getTile.isMoving)
                {
                    tilesAreMoving = true;
                    break;
                }
            }
            if (!tilesAreMoving) break;
        } while (true);
    }

    private MovePositions FindFarthestPositions(Vector2 vector, Vector2 cellPos)
    {
        Vector2 prevPos;
        MovePositions positions = new MovePositions();
        do
        {
            prevPos = cellPos;
            cellPos = new Vector2(prevPos.x + vector.x, prevPos.y + vector.y);
        } while (WithinBounds(cellPos) && IsCellAvailable(cellPos));
        positions.farthest = prevPos;
        positions.next = cellPos;
        return positions;
    }

    private void ScoreEventChanged(int value)
    {
        if (ScoreEvent != null)
        {
            m_Score += value;
            ScoreEvent(m_Score);
        }
    }

    private void GameOver()
    {
        if (GameOverEvent != null)
        {
            GameOverEvent();
        }
    }
}
