﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    private static Tile m_Prefab = null;

    public Color[] tileColors = new Color[12] {
        new Color(0.733f, 0.894f, 0.855f),
        new Color(0.729f, 0.878f, 0.784f),
        new Color(0.749f, 0.794f, 0.475f),
        new Color(0.761f, 0.684f, 0.388f),
        new Color(0.765f, 0.586f, 0.373f),
        new Color(0.715f, 0.499f, 0.331f),
        new Color(0.729f, 0.412f, 0.027f),
        new Color(0.729f, 0.302f, 0.381f),
        new Color(0.729f, 0.224f, 0.314f),
        new Color(0.729f, 0.150f, 0.247f),
        new Color(0.729f, 0.100f, 0.181f),
        new Color(0.235f, 0.025f, 0.196f)};

    public float moveDuration = 0.25f;
    public bool isMoving = false;
    public float duration = 0.25f;
    public float startScale = 0.0f;
    public float startDelay = 0.0f;
    public float endScale = 0.8f;

    public int value { get; set; }
    public bool merged { get; set; }
    private Vector2 previousPosition;
    private TextMesh textMesh = null;

    public static Tile prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<Tile>("Prefabs/Tile");
            }
            return m_Prefab;
        }
    }

    public void Awake()
    {
        textMesh = GetComponentInChildren<TextMesh>();
    }

    public void Create(Vector2 p_Position, int p_Value)
    {
        previousPosition = p_Position;
        ChangeValue(p_Value);
        merged = false;
        StartCoroutine(Scale());
    }

    public void ChangeValue(int p_Value)
    {
        value = p_Value;
        textMesh.text = value.ToString();
        GetComponent<SpriteRenderer>().color = tileColors[Mathf.RoundToInt(Mathf.Log(value, 2) - 1)];
        textMesh.color = value > 4 ? Color.white : Color.black; 
    }

    public IEnumerator Move(Vector2 destPos)
    {
        isMoving = true;
        Vector2 l_destPos = destPos;
        float time = 0.0f;
        do
        {
            time += Time.deltaTime;
            transform.position = (Vector3)Vector2.Lerp(previousPosition, destPos, time / moveDuration) + Vector3.zero;
            yield return null;
        } while (time < moveDuration);
        previousPosition = destPos;
        isMoving = false;
    }

    public IEnumerator Scale()
    {
        isMoving = true;
        Transform myTransform = transform;
        myTransform.localScale = startScale * Vector3.one;
        if (startDelay > 0.01f)
        {
            yield return new WaitForSeconds(startDelay);
        }
        float time = 0.0f;
        do
        {
            time += Time.deltaTime;
            myTransform.localScale = Mathf.SmoothStep(startScale, endScale, time / duration) * Vector3.one;
            yield return null;
        } while (time < duration);
        isMoving = false;
    }

    public IEnumerator Destroy()
    {
        isMoving = true;
        Transform myTransform = transform;
        myTransform.localScale = startScale * Vector3.one;
        if (startDelay > 0.01f)
        {
            yield return new WaitForSeconds(startDelay);
        }
        float time = 0.0f;
        do
        {
            time += Time.deltaTime;
            myTransform.localScale = Mathf.SmoothStep(endScale, startScale, time / duration) * Vector3.one;
            yield return null;
        } while (time < duration);
        myTransform.parent = null;
        StartCoroutine(DestroyLater());
        isMoving = false;
    }

    private IEnumerator DestroyLater()
    {
        yield return new WaitForSeconds(1.0f);
        Destroy(gameObject);
    }
}
