﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Text m_ScoreText;
    [SerializeField]
    private Button m_ButtonNewGame;
    [SerializeField]
    private GameObject m_GameOverPanel;
    [SerializeField]
    private Button m_RetryGameButton;

    public int m_StartTiles = 2;
    public int m_GridSize = 4;
    private Grid m_Grid;
    private bool m_GameOver = false;

    private Grid grid
    {
        get
        {
            if (m_Grid == null)
                m_Grid = FindObjectOfType<Grid>();
            return m_Grid;
        }
    }

    // Use this for initialization
    void Start()
    {
        Application.targetFrameRate = 60;
        AutoAspectRatio autoAspectRatio = FindObjectOfType<AutoAspectRatio>();
        autoAspectRatio.SetAspectRatio(m_GridSize);
        SwipeGesture swipeGesture = FindObjectOfType<SwipeGesture>();
        swipeGesture.SwipeEvent += new SwipeEventHandler(OnSwipe);
        grid.ScoreEvent += new ScoreEventHandler(OnScoreValueChanged);
        grid.GameOverEvent += new GameOverEventHandler(OnGameOver);
        SetUI();
        StartGame();
    }

    private void SetUI()
    {
        m_ButtonNewGame.onClick.AddListener(OnClickNewGame);
        m_RetryGameButton.onClick.AddListener(OnClickNewGame);
        m_GameOverPanel.gameObject.SetActive(false);
    }

    private void OnClickNewGame()
    {
        if (grid.IsGridBlocked)
            return;
        m_GameOverPanel.gameObject.SetActive(false);
        StartGame();
    }

    private void OnGameOver()
    {
        m_GameOver = true;
        Invoke("EnableGameOverPanel", 1.0f);
    }

    private void EnableGameOverPanel()
    {
        m_GameOverPanel.gameObject.SetActive(true);
    }

    private void OnScoreValueChanged(int value)
    {
        if(m_ScoreText != null)
        {
            m_ScoreText.text = value.ToString();
        }
    }

    public void StartGame()
    {
        StopAllCoroutines();
        m_GameOver = false;
        m_ScoreText.text = "0";
        grid.Clear();
        grid.Create(m_GridSize);
        AddStartTiles();
    }

    /// <summary>
    /// Создание начальных тайлов
    /// </summary>
    private void AddStartTiles()
    {
        for (int i = 0; i < m_StartTiles; i++)
        {
            m_Grid.AddRandomTile();
        }
    }

    /// <summary>
    /// Движение тайлов на игровом поле в заданном направлении
    /// </summary>
    /// <param name="direction">Направление движения тайлов</param>
    private void Move(Direction direction)
    {
        if (grid.IsGridBlocked)
            return;
        if (m_GameOver)
            return;
        StartCoroutine(grid.TileMove(direction));
    }

    private void OnSwipe(Direction direction)
    {
        Move(direction);
    }

    // Update is called once per frame
    void Update()
    {
        // Управление на PC 
        //#if UNITY_STANDALONE
        if (Input.GetKeyUp(KeyCode.UpArrow))
            Move(Direction.UP);
        if (Input.GetKeyUp(KeyCode.DownArrow))
            Move(Direction.DOWN);
        if (Input.GetKeyUp(KeyCode.LeftArrow))
            Move(Direction.LEFT);
        if (Input.GetKeyUp(KeyCode.RightArrow))
            Move(Direction.RIGHT);
        //#endif
    }
}
