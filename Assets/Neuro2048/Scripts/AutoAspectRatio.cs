﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAspectRatio : MonoBehaviour
{

    private float orthoSize = 0;
    private int minWidth = 2;
    private float orthoScaleCoeff = 1.85f;
    private Vector3 cameraDefaultPosition = new Vector3(0, 0, -10);
    [SerializeField]
    public bool mAlignWidth = true;

    public void SetAspectRatio(int pReferenceWorldUnits)
    {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        if (mAlignWidth)
            orthoSize = pReferenceWorldUnits * screenHeight / (orthoScaleCoeff * screenWidth);
        else
            orthoSize = pReferenceWorldUnits / orthoScaleCoeff;

        // obtain camera component so we can modify its viewport
        Camera camera = GetComponent<Camera>();
        float deltaOffset = (pReferenceWorldUnits / minWidth) + pReferenceWorldUnits % minWidth / 2.0f - 0.5f;
        Vector3 cameraOffset = new Vector3(deltaOffset, -deltaOffset);
        camera.transform.position = cameraDefaultPosition + cameraOffset;
        camera.orthographicSize = orthoSize;
    }

    public float GetOrthoSize
    {
        get
        {
            return orthoSize;
        }
    }
}
