﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public static Cell m_Prefab = null;
    public Vector2 position { get; set; }
    private Tile tile = null;

    public static Cell prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<Cell>("Prefabs/Cell");
            }
            return m_Prefab;
        }
    }

    public void SetTile(Tile newTile)
    {
        tile = newTile;
        if (tile == null) return;
        // Второй параметр SetParent отвечает за сохранение глобального положения в пространстве
        // При false глобальное позиционирование не сохраняется
        tile.transform.SetParent(transform, true); // true использована, чтобы сохранить сортировку слоев
        // Тайл позиционируется по месту расположения родительской клетки
        tile.transform.position = (Vector2)transform.position;
    }

    public Tile getTile
    {
        get
        {
            return tile;
        }
    }

    public bool tileExist
    {
        get
        {
            if (tile != null)
                return true;
            return false;
        }
    }
}
